#include "../../../Specialization/LP_Problems/Maintenance_Problem/Testing/maintenance_problem_testing.h"

#include <cmath>

int main(int argc, char** argv){
  assert(argc == 2);
  std::string path(argv[1]);

  execute_tests(path, true, false);
}
