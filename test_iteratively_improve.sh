#! /bin/bash

shopt -s extglob

models=("maintenance_problem_ip" "maintenance_problem_benders")
exec=$(find -wholename "./mk_tests/*" | grep -E -v '(.cpp|.o)$')

echo "tests loaded:"
echo "${exec}"
let count=20

mkdir data
mkdir data/test_data
mkdir out


while [ $count -lt 21 ]; do
  echo "executing for $count"
  for test in ${exec}; do
    if [ $1 == "manual" ]; then
      echo "position $test; execute?"
      read skip
      if [ "$skip" = "no" ]; then
        continue
      elif [ "$skip" != "yes" ]; then
	echo "aborting"
        exit 1
      fi
    fi

    columns=$(echo "$test" | rev | cut -d'/' -f1 | rev | cut -d'_' -f1,2 --output-delimiter=" ")
    test_filename=$(echo $test | rev | cut -d'/' -f1 | rev)

    mkdir data/test_data/$test_filename
    echo "executing $test data/test_data/$test_filename $count";
    bash -c "$test data/test_data/$test_filename $count > >(tee -a all_exec.log) 2> >(tee -a stderr.log >&2)"
    echo "executing ./mk_utils/gather_data_to_csv data/test_data/$test_filename $columns ${models[*]}"
    bash -c "./mk_utils/gather_data_to_csv data/test_data/$test_filename $columns ${models[*]} -o ./out/$test_filename 2> >(tee -a stderr.log >&2)"
  done
  make push

  #break
  let count=count+1;
done
