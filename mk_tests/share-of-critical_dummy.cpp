#include "../../../Specialization/LP_Problems/Maintenance_Problem/Testing/maintenance_problem_testing.h"
#include "../mk_utils/probe_test.cpp"
#include <cmath>

int main(int argc, char** argv){
  bool just_probe = false;
  size_t number_of_instances_per_coord = 0;

  assert(argc > 1);
  std::string path(argv[1]);
  if(path == "probe"){
    just_probe = true;
  }else{
    assert(argc == 3);
    number_of_instances_per_coord = std::atoi(argv[2]);
  }

  size_t nnodes = 40;
  size_t nedges = 250;

  std::function<maintenance_problem_generator(double, double)> test_generator = [&nnodes,&nedges](double share_of_critical, double dummy){
    return maintenance_problem_generator(
      random_graph_generator(
                    // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
        tipping_policy(false),
                      //   number_of_nodes      number_of_steps   node_attribute_generator
        uniform_node_steps(nnodes,                  1,                {{}}),
                      //          number_of_edges                    fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
        uniform_edge_step_fuzzing(nedges,         0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
        tipping_policy(true,             true,             false),
        //                 number_of_nodes number_of_steps   node_attribute_generator
        uniform_node_steps(1,              1,                {{}}),
        //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
        uniform_edge_step_fuzzing(0,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
        {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
        {{"Nodepotential", Attribute(min, 0)}},
        {{{"Upper", {fix, Integral, 1, 100}}}},
        {},
        {{{"Upper", {fix, Integral, 1, 100}}}}
      ),
      //                                   critical_edge_candidates
      3, share_of_critical, everywhere
    );
  };

  if(just_probe){
    nnodes = 20;
    nedges = 60;
    probe(test_generator(.5,0));
  }

  return generate_and_execute_2d_plot_test(path, number_of_instances_per_coord, axis_data{"share-of-critical", 9, .2, 1}, axis_data{"dummy", 1, 0, 0}, test_generator);
}
