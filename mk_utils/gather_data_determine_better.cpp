#include <fstream>
#include <vector>
#include <filesystem>

#include "../../../Specialization/LP_Problems/Maintenance_Problem/Testing/maintenance_problem_testing.h"

// make sure the requested models actually exist in the parsed data. Otherwise this in csv(...) at the latest.
int main(int argc, char** argv){
  assert(argc == 4);
  std::filesystem::path top_level_path(argv[1]);

  std::string modela(argv[2]);
  std::string modelb(argv[3]);

  std::vector<Data> data_vector;
  gather_data(top_level_path, data_vector);


  size_t win_counter = 0;
  size_t win_counter_more_than_1_sec = 0;
  size_t counter = 0;
  size_t counter_more_than_1_sec = 0;

  for(Data data : data_vector){
    auto modela_search = data.derived_performance.find(modela);
    auto modelb_search = data.derived_performance.find(modelb);
    assert(modela_search != data.derived_performance.end());
    assert(modelb_search != data.derived_performance.end());
    auto modela_perf_vec = modela_search->second;
    auto modelb_perf_vec = modelb_search->second;
    assert(modela_perf_vec.size() == 1);
    assert(modelb_perf_vec.size() == 1);
    assert(modela_perf_vec[0].values_complete() && modelb_perf_vec[0].values_complete());
    if(modela_perf_vec[0].time_in_sec.second < modelb_perf_vec[0].time_in_sec.second){
      ++win_counter;
    }
    ++counter;
    if(modelb_perf_vec[0].time_in_sec.second > 1 || modela_perf_vec[0].time_in_sec.second > 1){
      if(modela_perf_vec[0].time_in_sec.second < modelb_perf_vec[0].time_in_sec.second){
        ++win_counter_more_than_1_sec;
      }
      ++counter_more_than_1_sec;
    }
  }

  std::cout << modela << " wins " << (double)win_counter/counter*100 << "% of the times and " << (double)win_counter_more_than_1_sec/counter_more_than_1_sec*100 << "% of the times when discarding instances of less than 1 sec run time in both of the models which are " << (double)counter_more_than_1_sec/counter*100 << "% of instances" << std::endl;
}
