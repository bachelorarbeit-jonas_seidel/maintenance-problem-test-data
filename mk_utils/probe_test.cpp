#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cassert>
#include <vector>
#include <climits>

#include "../../../Specialization/LP_Problems/Maintenance_Problem/Maintenance_Problem.h"
#include "../../../Specialization/LP_Problems/Maintenance_Problem/maintenance_problem_generator.h"

void probe(maintenance_problem_generator mpg){
  Maintenance_Problem mp;
  while(true){
    system("killall graph_display");
    std::cout << "generating problem" << std::endl;
    mp = mpg.next();
    std::cout << "generation finished" << std::endl;

    std::ofstream ofs_netw("../tmp.netw");
    ofs_netw << mp.network() << std::endl;
    ofs_netw.close();
    std::cout << static_cast<Linear_Program>(mp) << std::endl;
    std::cout << mp.network() << std::endl;
    std::cout << "finished write" << std::endl;
    system("cd .. && (./graph_display --file ./tmp.netw &) && cd mk_tests");

    std::cout << "generating SCIP model" << std::endl;
    std::vector<SCIP*> scips = mp.all_computational_models();
    for(SCIP* scip : scips){
      std::cout << "---------------->" << SCIPgetProbName(scip) << ":" << std::endl;
      SCIPsolve(scip);
    }

    for(SCIP* scip : scips){
      SCIPfree(&scip);
      //std::cin.ignore();
    }
    //system("polymake --script .data/Maintenance_Problem/pm_script_lp2facets");
    std::cin.ignore();
  }
}
